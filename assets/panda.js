function isHomePage() {
   if (window.location.href == 'https://slickthemes.org/'){
      return true      
   } else {
      return false
   }
}

// Trigger animation
$(document).ready(function(){
   if (!isHomePage()){
      return
   }
   var animationComplete = false;
   document.addEventListener("mouseleave", function(event){
      if(!animationComplete && (event.clientY <= 0 || event.clientX <= 0 || (event.clientX >= window.innerWidth || event.clientY >= window.innerHeight)))
      {
         var $slider = document.getElementById('slider');
         $slider.setAttribute('class', 'slide-in');
      }
   });

   document.getElementById('slider').addEventListener("mouseover", function(event){
      var $slider = document.getElementById('slider');
      $slider.setAttribute('class', 'slide-out');      
      // Only display animation once 
      animationComplete = true;
   });

});