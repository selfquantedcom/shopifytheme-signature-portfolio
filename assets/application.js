// Put your applicaiton javascript here

// Style links in links dynamically created by users (the links are created by the user so cannot be styled directly ... without copying and overwritting all the Bootstrap styles)
$(document).ready(function(){
   $('.jumbotron a').addClass('col-xs-12 col-sm-12 col-md-6 col-lg-4 centered-content btn btn-secondary btn-lg btn-block text-truncate mt-auto text-color-light');  
   $('.row-carousel-text a').addClass('col-xs-12 col-sm-12 col-md-6 col-lg-4 btn btn-secondary btn-lg btn-block text-truncate mt-auto text-color-light');      
});

// This is a hack to replace link on product pages.
$(document).ready(function(){
   if(window.location.href == 'https://slickthemes.org/products/portfolio-theme') {
      $('#product-link').attr("href", "https://plain-company.myshopify.com/"); 
   } else if (window.location.href == 'https://slickthemes.org/products/portfolio-tree') {
      $('#product-link').attr("href", "https://plain-ecommerce.myshopify.com/"); 
   } else if (window.location.href == 'https://slickthemes.org/products/professional-practice') {
      $('#product-link').attr("href", "http://www.stomatochirurgie-cevelova.cz/"); 
   }         
});

