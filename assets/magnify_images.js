// Open a dialog window with large image when user click on an image in a blog article
$(document).ready(function(){
   // Get all images & skip the first as this will either be a brand icon when present or the preview img when not brand icon exists. Either way it is ok to skip those.
   var container = document.getElementsByClassName('container')[0];
   var images = container.querySelectorAll('img');   
   var modals = [];

   for (var i = 0; i < images.length; i++) {
      
      var imgSrc = images[i].src;      
      var htmlString = `<div class="modal" id="image_modal_${i}"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Image</h4><span data-dismiss="modal" class="modal-close-btn"><span aria-hidden="true">&times;</span></span></div><div class="modal-body"><div class="container"><div class="row justify-content-center text-center centered-content mb-3"><img class="" src="${imgSrc}"/></div></div></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button></div></div></div></div>`;
      images[i].classList.add("pointer");
      modals.push(htmlString);

      // I is interpreted at the time when the event listener is triggered; i will always take the last value
      images[i].addEventListener("click", function(event){      
         var targetSrc = event.target.src;
         var container = document.getElementsByClassName('container')[0];
         var images = container.querySelectorAll('img');
         
         for (var i = 0; i < images.length; i++) {
            if(images[i].src === targetSrc) {
               $("#image_modal_" + String(i)).modal();
            }
         }
      });           
   }  
   // Only insert modals after the first loop otherwise the new images may be captured by the loop
   for (var i = 0; i < modals.length; i++) {
      document.body.insertAdjacentHTML( 'beforeend', modals[i] );
   }

} );

$(document).ready(function(){
   $('.container img').addClass('max-full-width');     
});

